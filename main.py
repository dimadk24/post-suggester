import re
from time import sleep

import requests
import sys
from decouple import config

access_token = config('access_token', default=None)
filenames = ['text', 'attachments', 'publics']
api_url = 'https://api.vk.com/method/'
api_version = '5.71'

retry_errors = {
    1: 'Произошла неизвестная для ВК ошибка',
    6: 'Слишком много запросов в секунду',
    10: 'Произошла внутрення ошибка сервера ВК'
}

stop_errors = {
    5: 'Авторизация не удалась',
    9: 'Слишком много однотипных действий',
    203: 'Доступ к группе запрещен',
    220: 'Слишком много получателей для 1 поста'
}

no_access_errors = {
    15: 'Невозможно предложить пост в эту группу. У них отключена предложка. Я ее пропущу',
    214: 'Нет доступа к стене паблика. Скорее всего они добавили тебя в черный список. Я его пропущу'
}


def close():
    input('Скрипт завершен. Нажми Enter для выхода или закрой это окно...\n')
    sys.exit()


def request(url, **kwargs):
    working_data = kwargs.copy()
    working_data.update({'access_token': access_token, 'v': api_version})
    r = requests.post(url=url, data=working_data)
    json_response = r.json()
    if json_response.get('error'):
        error_code = json_response['error']['error_code']
        if error_code in stop_errors:
            print(stop_errors[error_code])
            close()
        elif error_code in retry_errors:
            sleep(0.45)
            return request(url, **kwargs)
        elif error_code in no_access_errors:
            print(no_access_errors[error_code])
            return False
        else:
            print('Произошла ошибка, отсутствующая в базе ошибок')
    else:
        return json_response['response']


print('Поддержка:', 'vk.com/dimadk24', 't.me/dimadk24', 'vk.com/id159204098', sep='\n', end='\n\n')

# Get data from text files
data = {}
for filename in filenames:
    with open(filename + '.txt') as f:
        data.update({filename: f.read()})

# Split file data in lines
data['attachments'] = data['attachments'].split('\n')
data['publics'] = data['publics'].split('\n')

print('Текст:', data['text'], sep='\n')
print('Приложения к посту:')

# Resolve attachments
i = 0
for attachment in data['attachments']:
    attachment = attachment.strip()
    if attachment:
        if not (('z=' in attachment and '%2F' in attachment) or ('vk.com/' in attachment)):
            print('Неверная ссылка или неверный формат:', attachment)
            close()
        if 'z=' in attachment and '%2F' in attachment:
            attachment = attachment[attachment.index('z=')+2: attachment.index('%2F')]
        else:
            attachment = attachment[attachment.index('vk.com/')+7:]
        data['attachments'][i] = attachment
        print(attachment)
        i += 1
data['attachments'] = ','.join(data['attachments']).strip(',')

# Trim publics to 59
# if len(data['publics']) > 59:
#     del data['publics'][59: len(data['publics'])]

print('Паблики для предложения:')

# Resolve publics
regexs = {'public': re.compile('^public[\d]+$'),
          'club': re.compile('^club[\d]+$')}
i = 0
for public in data['publics']:
    public = public.strip()
    if public:
        if 'vk.com/' in public:
            public = public.partition('vk.com/')[2]
            if regexs['public'].match(public):
                public = '-' + public[public.index('public')+6:]
            elif regexs['club'].match(public):
                public = '-' + public[public.index('club')+4:]
            else:
                response = request(api_url + 'utils.resolveScreenName', screen_name=public)
                if response and response['type'] in ('group', 'page'):
                    public = '-' + str(response['object_id'])
                else:
                    print('Неверная ссылка на паблик:', public, 'Она ссылается не на сообщество. Я пропущу эту ссылку')
                    public = None
                del response
        elif re.match('^[\d]+$', public):
            public = '-' + public
        elif re.match('^-[\d]+$', public):
            pass
        else:
            print('Неверный формат паблика:', public, 'Я его пропущу')
            public = None
        data['publics'][i] = public
        if public:
            print(public)
        i += 1

# print(data)
print('Всего пабликов для предложения поста:', len(data['publics']))

# Suggest posts
i = 0
for public in data['publics']:
    if public:
        response = request(api_url + 'wall.post',
                           owner_id=public,
                           message=data['text'],
                           attachments=data['attachments'])
        if response and response.get('post_id'):
            i += 1
            print(i, 'предложил пост в', public)
        else:
            print('Возникла ошибка предложения поста в {public}'
                  .format(public=public))
        if i == 59:
            print('Достигнуто максимальное количество предложенных постов:', i)
            break
print('Всего предложил постов: {n}'.format(n=i))
close()
